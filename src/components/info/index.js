import React from "react"
import styled from "styled-components"

const GridContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  grid-column-gap: 25px;
  grid-row-gap: 0px;
  align-items: center;
  justify-items: center;
  min-height: 100vh;
`

const StyledIframe = styled.iframe`
  border: none;
`

const ItemContainer = styled.div`
  width: 100%;
`

const StyledHeader = styled.h1`
  margin: 0px;
  padding-top: 25px;
`

const Info = () => {
  return (
    <>
      <StyledHeader>Praktisk Info</StyledHeader>
      <br/>
      <address><strong>Toastmaster</strong>
        <p>Mads Dyrmann</p>
        <p>Tlf.: <a href="tel:31170017">31 17 00 17</a></p>
          <p>E-mail: <a href="mailto:mads@dyrmann.com">mads@dyrmann.com</a></p>
          <br/>
        </address>
      <GridContainer>
        <ItemContainer>
          <h2>13.00 Vielsen</h2>
          <hr width="80%" />
          <p>
            Først skal vi vies. Det foregår nede i klitterne nær familiens
            sommerhus, hvor selveste Fanøs Borgmester står for ceremonien.
            Derefter vil der være nogle forfriskninger.
          </p>
          <address>Adressen er: Klitkrogen 19 - Det er muligt at tage en bus til Fanø Bad og derfra gå ca. 1 km ud til sommerhuset.</address>
          <strong>NB: Der må ikke parkeres eller køres ind ved klitkrogen, da passagen er meget smal. Man opfordres i stedet til at parkere på golfstien og gå op ad selve Klitkrogen.</strong>
          <br />
          <br />
          <StyledIframe
            title="vielsen"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11045.665648619479!2d8.363203316128313!3d55.43896412401302!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x464ad8906cf92411%3A0x922edce1f361ae5c!2sKlitkrogen%2019%2C%206720%20Fan%C3%B8!5e1!3m2!1sen!2sdk!4v1616512251875!5m2!1sen!2sdk"
            allowfullscreen=""
            loading="lazy"
          ></StyledIframe>
          <figcaption>(Klik på kortet for at åbne i Googe Maps)</figcaption>
        </ItemContainer>
        <ItemContainer>
          <h2>15.00 Festen</h2>
          <hr width="80%" />
          <p>
            Selve festen holdes i Strien (Fanø Håndværker- og Industiforening).
          </p>
          <address>Adressen hertil er: Lindevej 5-7 - Her er parkeringsmuligheder.</address>
          <br />
          <StyledIframe
            title="festen"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11044.507420245793!2d8.375176697195196!3d55.44310258781003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x464b27426366eb55%3A0xc8668cda569f5eee!2sFan%C3%B8%20H%C3%A5ndv%C3%A6rker-%20Og%20Industriforening!5e1!3m2!1sen!2sdk!4v1616512098256!5m2!1sen!2sdk"
            allowfullscreen=""
            loading="lazy"
          ></StyledIframe>
          <figcaption>(Klik på kortet for at åbne i Googe Maps)</figcaption>
        </ItemContainer>
      </GridContainer>
    </>
  )
}

export default Info
