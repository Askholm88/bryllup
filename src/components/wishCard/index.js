import React, { useState } from "react"
import { Card } from "semantic-ui-react"
import styled from "styled-components"
import { storage } from "../../index"

const StyledCard = styled(Card)`
    box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
    transition: all 0.3s cubic-bezier(.25,.8,.25,1);
    margin: 0 !important;
    box-shadow: 
    border-radius: 5px;
    height: 100%;
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`

const StyledImage = styled.img`
  max-width: 100%;
`

const StyledContent = styled.div``

const Wish = ({ wish }) => {
  const [image, setImage] = useState("")
  storage
    .ref(`images/${wish.image}`)
    .getDownloadURL()
    .then((result) => setImage(result))

  return (
    <StyledCard>
      <StyledImage src={image} wrapped ui={false} />
      <StyledContent>
        <Card.Header>{wish.header}</Card.Header>
        {wish.url && (
          <Card.Meta>
            <span>
              <a href={wish.url}>Link til eksempel</a>
            </span>
          </Card.Meta>
        )}
        <Card.Description>{wish.desc}</Card.Description>
      </StyledContent>
    </StyledCard>
  )
}

export default Wish
