import React, { useState, useEffect } from "react"
import styled from 'styled-components'
import Wish from '../wishCard'
import { Button, Modal } from 'semantic-ui-react'
import { db, storage } from '../../index'
import 'semantic-ui-css/semantic.min.css'


const GridContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(290px, 1fr));
    grid-column-gap: 25px;
    grid-row-gap: 25px; 
    align-items: center;
    justify-items: center;
`

const GiftMasterAlert = ({ isOpen, setIsOpen }) => {

    return (
        <Modal
        open={isOpen}
      >
        <Modal.Header>OBS!</Modal.Header>
        <Modal.Content>
          <p><strong>Gavekoordinator: </strong><i>Jakob Roer Askholm</i>, <a href="tel:22388559">22 38 85 59</a>, <a href="mailto:jakobaskholm@gmail.com">jakobaskholm@gmail.com</a></p>
          <p><strong>Toastmaster: </strong><i>Mads Dyrman</i>, <a href="tel:31170017">31 17 00 17</a>, <a href="mailto:mads@dyrmann.com">mads@dyrmann.com</a></p>
        </Modal.Content>
        <Modal.Actions>
          <Button positive onClick={() => setIsOpen(!isOpen)}>
            Ok
          </Button>
        </Modal.Actions>
      </Modal>
    )
}

const WishList = () => {
    const [wishList, setWishList] = useState([])
    const [images, setImages] = useState([])
    const [isOpen, setIsOpen] = useState(true)
  
    const fetchWishList = async() => {
      db.collection('wishList')
      .get()
      .then(snapshot => {
        const data = snapshot.docs.map((doc => doc.data()))
        setWishList(data)
      })
    }

    const fetchImages = async() => {
        const response = storage.ref("images")
        response.listAll().then((result) => {
            result.items.forEach(async(imageRef) => {
                const imageURL = await imageRef.getDownloadURL()
                setImages([...images, imageURL])
            })
        })
       
        //.getDownloadURL().then((result) => setImages(result)) 
    }
    
    useEffect(() => {
      fetchWishList()
      fetchImages()
    }, [])
  
    return (
        <>
        <GiftMasterAlert isOpen={isOpen} setIsOpen={setIsOpen} />
        <h1>Ønskeliste</h1>
        <address><strong>Gavekoordinator</strong><br/>
        <p>Jakob Roer Askholm</p>
        <p>Tlf.: <a href="tel:22388559">22 38 85 59</a></p>
          <p>E-mail: <a href="mailto:jakobaskholm@gmail.com">jakobaskholm@gmail.com</a></p>
          <br/>
        </address>
        <GridContainer>
            {wishList && wishList.map((current, index) => {
                return(
                    <Wish key={current.header+index} wish={current} />
                )
            })}
        </GridContainer>
    </>)
}

export default WishList