import React from "react"
import { Tab } from "semantic-ui-react"
import WishList from '../wishList'
import Wedding from "../../assets/images/wedding.jpeg"
import Info from '../info'
import styled from 'styled-components'
import SaveTheDate from '../../assets/images/savethedate.jpg'

const StyledPane = styled(Tab.Pane)`
    background: transparent !important;
    border: none !important;
    min-height: 100vh;
`

const StyledTab = styled(Tab)`
    .ui.menu {
        margin-top: 100%;
        border-radius: 5px;
        width: 100% !important;
    }
`

const MainMenu = ({className}) => {
    const panes = [
        {
            menuItem: 'Find vej',
            render: () => <StyledPane><Info /></StyledPane>,
        },
        { menuItem: 'Info', render: () => <StyledPane className={className}><img width="600px" alt="intro" src={SaveTheDate}/></StyledPane> },
        { menuItem: 'Ønskeliste', render: () => <StyledPane><WishList /></StyledPane> },
        { menuItem: 'Billeder', render: () => <StyledPane><img alt="wedding" src={Wedding}/></StyledPane> },
      ]
      
      return(<StyledTab className={className} menu={{ color: "blue", vertical: true, attached: true}} panes={panes} />)
      

}

export default MainMenu