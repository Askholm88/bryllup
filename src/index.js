import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import firebase from 'firebase'
import styled from 'styled-components'

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCGtDY28-GjL0RrixX3UnWHyklzHajoOuM",
  authDomain: "lynvardbryllup.firebaseapp.com",
  projectId: "lynvardbryllup",
  storageBucket: "lynvardbryllup.appspot.com",
  messagingSenderId: "317826534479",
  appId: "1:317826534479:web:743e520530aa0fb8e02e3a",
  measurementId: "G-R6EHDK8LZG"
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

export const db = firebase.firestore()
export const storage = firebase.storage()

const StyledApp = styled(App)`
  box-sizing: border-box;
`


ReactDOM.render(
  <React.StrictMode>
    <StyledApp />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
