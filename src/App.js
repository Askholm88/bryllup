import "./App.css"
import styled from "styled-components"
import Background from "./assets/images/background.jpg"
import Info from "./components/info"
import WishList from "./components/wishList"
import React, { useState } from "react"

const StyledContainer = styled.div`
  font-family: "Nunito Sans", sans-serif;
  position: relative;
  @media (max-width: 375px) {
    padding-bottom: 75px;
  }
`

const BackgroundImage = styled.div`
  content: " ";
  display: block;
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  opacity: 0.25;
  background-image: url(${Background});
  background-repeat: no-repeat;
  background-position: 50% 0;
  background-size: cover;
  z-index: -1;
`

const Navbar = styled.nav`
  background-image: linear-gradient(to bottom right, rgb(67, 60, 84), rgb(200, 193, 219));
  overflow: hidden;
  height: 75px;
  display: flex;
  z-index: 2;
  justify-content: space-evenly;
  align-items: center;
  @media (max-width: 375px) {
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
  }
`

const StyledLink = styled.a`
  cursor: pointer;
  color: white;
  text-align: center;
  text-decoration: none;
  font-size: 1.6em;
  align-self: center;
  :hover {
    color: black;
    text-decoration: underline;
  }
`

function App() {
  const [displayContent, setDisplayContent] = useState(<WishList />)

  return (
    <StyledContainer className="App">
      <Navbar>
        <StyledLink onClick={() => setDisplayContent(<Info />)}>
          Info
        </StyledLink>
        <StyledLink onClick={() => setDisplayContent(<WishList />)}>
          Ønskeliste
        </StyledLink>
      </Navbar>
      <BackgroundImage />
      {displayContent}
    </StyledContainer>
  )
}

export default App
